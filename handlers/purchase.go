package handlers

import (
	"log"
	"net/http"

	"../models"
	"../types"
	"github.com/labstack/echo"
)

// CreatePurchase - create new purchase
func CreatePurchase(c echo.Context) error {

	var req types.PurchaseCreateReq
	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers/CreateCompany #1", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	res, err := models.CreatePurchase(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR.")
	}

	return c.JSON(http.StatusCreated, res)
}
