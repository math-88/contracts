package handlers

import (
	"errors"
	"log"
	"net/http"
	"strconv"

	"../models"
	"../types"
	"github.com/labstack/echo"
)

// CreateContract - create new contract
func CreateContract(c echo.Context) error {

	var req types.ContractCreateReq
	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers/CreateContract #1", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	if (req.SellerCompany < 1) || (req.ClientCompany < 1) {
		log.Println("ERROR. Company id error")
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	res, err := models.CreateContract(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR.")
	}

	return c.JSON(http.StatusCreated, res)
}

// GetContractList - get companies list
func GetContractList(c echo.Context) error {

	var req types.ContractListReq

	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers/GetContractList #1", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	res, err := models.GetContractList(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR. Not found.")
	}

	return c.JSON(http.StatusOK, res)

}

// GetContractDetail - get contract info
func GetContractDetail(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	if id < 1 {
		return c.JSON(http.StatusBadRequest, "Error id value")
	}
	uid := uint(id)

	res, err := models.GetContractDetail(&uid)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR. Not found.")
	}

	return c.JSON(http.StatusOK, res)
}

// UpdateContract - update contract
func UpdateContract(c echo.Context) error {

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {

	}

	if id < 1 {
		err := errors.New("Error id")
		log.Println("ERROR. handlers.UpdateContract #1", err)
		return c.JSON(http.StatusBadRequest, "Error id")
	}

	var req types.ContractUpdateReq
	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers.UpdateContract #2", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	if req.SellerCompany == req.ClientCompany {
		err := errors.New("Error. Equal values ids for seller and client")
		log.Println("ERROR. handlers.UpdateContract #3", err)
		return c.JSON(http.StatusBadRequest, "Error. Equal values ids for seller and client")
	}

	res, err := models.UpdateContract(&req, &id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR")
	}

	return c.JSON(http.StatusCreated, res)
}

// DeleteContract - delete contract
func DeleteContract(c echo.Context) error {

	id, _ := strconv.Atoi(c.Param("id"))

	res, err := models.DeleteContract(&id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR. Not found.")
	}

	return c.JSON(http.StatusOK, res)
}
