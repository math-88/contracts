package handlers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/labstack/echo"

	"../models"
	"../types"
)

// CreateCompany - create new company
func CreateCompany(c echo.Context) error {

	var req types.CompanyCreateReq
	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers/CreateCompany #1", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	res, err := models.CreateCompany(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR.")
	}

	return c.JSON(http.StatusCreated, res)
}

// GetCompanyList - get companies list
func GetCompanyList(c echo.Context) error {

	var req types.CompanyListReq

	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers/GetCompanyList #1", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	res, err := models.GetCompaniesList(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR. Not found.")
	}

	return c.JSON(http.StatusOK, res)

}

// GetCompanyDetail - get company info
func GetCompanyDetail(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))

	res, err := models.GetCompanyDetail(&id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR. Not found.")
	}

	return c.JSON(http.StatusOK, res)
}

// UpdateCompany - update company
func UpdateCompany(c echo.Context) error {

	id, _ := strconv.Atoi(c.Param("id"))

	var req types.CompanyUpdateReq
	if err := c.Bind(&req); err != nil {
		log.Println("ERROR. handlers/UpdateCompany #1", err)
		return c.JSON(http.StatusBadRequest, "Error data format")
	}

	res, err := models.UpdateCompany(&req, &id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR.")
	}

	return c.JSON(http.StatusCreated, res)
}

// DeleteCompany - delete company
func DeleteCompany(c echo.Context) error {

	id, _ := strconv.Atoi(c.Param("id"))

	res, err := models.DeleteCompany(&id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "ERROR. Not found.")
	}

	return c.JSON(http.StatusOK, res)
}
