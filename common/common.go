package common

import (
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/joho/godotenv"
)

// DBtype - database param connecting
type DBtype struct {
	Address string
	Port    string
	Name    string
	User    string
	Pass    string
}

// Layout - time format
const Layout = "2006-01-02T15:04:05.000Z"

var (
	// ServerPort - golang server port
	ServerPort string

	// DB - database param
	DB DBtype
)

// Init - initialization
func Init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("ERROR Init #1. Error loading .env file")
	}
	ServerPort = os.Getenv("SERVER_PORT")
	DB.Address = os.Getenv("DB_ADDRESS")
	DB.Port = os.Getenv("DB_PORT")
	DB.Name = os.Getenv("DB_NAME")
	DB.User = os.Getenv("DB_USER")
	DB.Pass = os.Getenv("DB_PASS")

	rand.Seed(time.Now().UnixNano())
}

// TimePrint get time Now string
func TimePrint() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

const (
	symbKey = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	lenKey  = 6

	// for fast random generation
	letterIdxBits = 6
	letterIdxMask = 1<<letterIdxBits - 1
	letterIdxMax  = 63 / letterIdxBits
)

// GetRand - get random string
func GetRand() string {

	b := make([]byte, lenKey)
	for i, cache, remain := lenKey-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(symbKey) {
			b[i] = symbKey[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
