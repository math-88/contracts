package db

import (
	"log"
	"strings"

	"../types"
)

///////////////////////////////
/////////// company ///////////
///////////////////////////////

// CreateCompany - get companies list from DB
func CreateCompany(cReq *types.Company) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.CreateCompany #1", err)
		return
	}
	defer db.Close()

	err = db.Create(cReq).Error
	return
}

// GetCompanyList - get companies list from DB
func GetCompanyList(clReq *types.CompanyListReq) (companies []types.Company, err error) {

	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.GetCompanyList #1", err)
		return
	}
	defer db.Close()

	// db.LogMode(true)
	// db = db.Set("gorm:auto_preload", true)

	whereQueryArr := []string{"id > ?"}
	whereArgs := []interface{}{"0"}
	order := "id asc"

	if strings.EqualFold(clReq.Filter.OrderBy, "desc") {
		order = "id desc"
	}

	if clReq.Filter.Code != "" {
		whereQueryArr = append(whereQueryArr, "code = ?")
		whereArgs = append(whereArgs, clReq.Filter.Code)
	}
	if clReq.Filter.SearchWord != "" {
		whereQueryArr = append(whereQueryArr, "name LIKE ?")
		whereArgs = append(whereArgs, "%"+clReq.Filter.SearchWord+"%")
	}
	whereQuery := strings.Join(whereQueryArr, " AND ")

	// db.Order(order).Where(whereQuery, whereArgs...).Offset((clReq.Filter.Page - 1) * clReq.Filter.Limit).Limit(clReq.Filter.Limit).Find(&companies)
	err = db.Order(order).Where(whereQuery, whereArgs...).Find(&companies).Error

	return
}

// GetCompanyDetail - get company detail
func GetCompanyDetail(id *int) (company types.Company, err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.GetCompanyDetail #1", err)
		return
	}
	defer db.Close()

	db = db.Set("gorm:auto_preload", true)

	err = db.First(&company, *id).Error

	// if company.ID == 0 {
	// 	err = errors.New("Not found")
	// }

	return
}

// UpdateCompany - update company in db
func UpdateCompany(company *types.Company) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.CreateCompany #1", err)
		return
	}
	defer db.Close()

	// db.LogMode(true)

	mapa := make(map[string]interface{})
	if company.Name != "" {
		mapa["name"] = company.Name
	}
	mapa["code"] = company.Code

	err = db.Model(&company).Updates(mapa).Error
	return
}

// DeleteCompany - get company detail
func DeleteCompany(company *types.Company) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.DeleteCompany #1", err)
		return
	}
	defer db.Close()

	err = db.Delete(&company).Error

	return
}
