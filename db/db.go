package db

import (
	"fmt"
	"log"

	"../common"
	"../types"

	"github.com/jinzhu/gorm"
)

// Connect - connect to database
func Connect() (*gorm.DB, error) {
	//open a db connection
	connectString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True", common.DB.User, common.DB.Pass, common.DB.Address, common.DB.Port, common.DB.Name)

	db, err := gorm.Open("mysql", connectString)

	if err != nil {
		fmt.Println("ARARM!!")
	}

	return db, err
}

// Migrate - auto migration ON
func Migrate() {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Migrate #1", err)
		return
	}

	defer db.Close()

	db.AutoMigrate(&types.Purchase{}, &types.Contract{}, &types.Company{})
}
