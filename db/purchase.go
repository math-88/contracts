package db

import (
	"log"

	"../types"
)

// CreatePurchase - get Purchases list from DB
func CreatePurchase(pReq *types.Purchase) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.CreatePurchase #1", err)
		return
	}
	defer db.Close()

	err = db.Create(pReq).Error
	return
}

// GetUsedCredits - get used credits for contract
func GetUsedCredits(id *uint) (sum int, err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.GetUsedCredits #1", err)
		return
	}
	defer db.Close()

	// db.LogMode(true)

	type scan struct {
		Total int
	}
	var getScan scan

	err = db.Model(&types.Purchase{}).Where("contract_id = ?", *id).Select("sum(credits) as total").Scan(&getScan).Error
	if err != nil {
		log.Println("ERROR db.GetUsedCredits #2", err)
		return
	}

	sum = getScan.Total

	return
}
