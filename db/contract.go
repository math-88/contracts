package db

import (
	"log"
	"strings"

	"../common"
	"../types"
)

// CreateContract - get contracts list from DB
func CreateContract(cReq *types.Contract) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.CreateContract #1", err)
		return
	}
	defer db.Close()

	// db.LogMode(true)

	err = db.Create(cReq).Error
	return
}

// GetContractList - get companies list from DB
func GetContractList(clReq *types.ContractListReq) (companies []types.Contract, err error) {

	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.GetContractList #1", err)
		return
	}
	defer db.Close()

	// db.LogMode(true)
	// db = db.Set("gorm:auto_preload", true)

	whereQueryArr := []string{"id > ?"}
	whereArgs := []interface{}{"0"}
	order := "id asc"

	if strings.EqualFold(clReq.Filter.OrderBy, "desc") {
		order = "id desc"
	}

	if clReq.Filter.Number != "" {
		whereQueryArr = append(whereQueryArr, "code = ?")
		whereArgs = append(whereArgs, clReq.Filter.Number)
	}

	whereQuery := strings.Join(whereQueryArr, " AND ")

	// db.Order(order).Where(whereQuery, whereArgs...).Offset((clReq.Filter.Page - 1) * clReq.Filter.Limit).Limit(clReq.Filter.Limit).Find(&companies)
	err = db.Order(order).Where(whereQuery, whereArgs...).Find(&companies).Error

	return
}

// GetContractDetail - get contract detail
func GetContractDetail(id *uint) (contract types.Contract, err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.GetContractDetail #1", err)
		return
	}
	defer db.Close()

	db = db.Set("gorm:auto_preload", true)

	err = db.First(&contract, *id).Error

	// if contract.ID == 0 {
	// 	err = errors.New("Not found")
	// }

	return
}

// UpdateContract - update contract in db
func UpdateContract(contract *types.Contract) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.CreateContract #1", err)
		return
	}
	defer db.Close()

	// db.LogMode(true)

	mapa := make(map[string]interface{})
	if contract.Number != "" {
		mapa["number"] = contract.Number
	}
	if contract.SellerCompanyID > 0 {
		mapa["seller_company_id"] = contract.SellerCompanyID
	}
	if contract.ClientCompanyID > 0 {
		mapa["client_company_id"] = contract.ClientCompanyID
	}
	mapa["credits"] = contract.Credits

	err = db.Model(&contract).Updates(mapa).Error
	return
}

// DeleteContract - get contract detail
func DeleteContract(contract *types.Contract) (err error) {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.DeleteContract #1", err)
		return
	}
	defer db.Close()

	err = db.Delete(&contract).Error

	return
}

// GenerateContractKey - generate uniq key
func GenerateContractKey() (key string, err error) {

	var (
		free bool
	)
	// finding free keyName
	for !free {
		key = common.GetRand()
		free, err = CheckKey(key)
		if err != nil {
			return
		}
	}
	return

}

// CheckKey - check key
func CheckKey(key string) (free bool, err error) {

	db, err := Connect()
	if err != nil {
		log.Println("ERROR Connect db.CreateCompany #1", err)
		return
	}
	defer db.Close()

	var count int
	db.Model(&types.Contract{}).Where("number = ?", key).Count(&count)

	if err != nil {
		log.Println("ERROR CheckKey #1.", err)
		return
	}
	if count == 0 {
		free = true
	}

	return

}
