package db

import (
	"log"

	"../types"
)

// NewDB - delete and create new DB
func NewDB() {
	db, err := Connect()
	if err != nil {
		log.Println("ERROR NewDB #1", err)
		return
	}

	defer db.Close()

	// db.LogMode(true)

	// drop tables
	err = db.DropTableIfExists(&types.Purchase{}, &types.Contract{}, &types.Company{}).Error
	if err != nil {
		log.Fatal(err)
	}

	// create tables
	err = db.CreateTable(&types.Company{}, &types.Contract{}, &types.Purchase{}).Error
	if err != nil {
		log.Fatal(err)
	}

	// create foreignKeys
	db.Model(&types.Purchase{}).AddForeignKey("contract_id", "contracts(id)", "CASCADE", "CASCADE")
	db.Model(&types.Contract{}).AddForeignKey("seller_company_id", "companies(id)", "CASCADE", "CASCADE")
	db.Model(&types.Contract{}).AddForeignKey("client_company_id", "companies(id)", "CASCADE", "CASCADE")

}
