package types

// TimeExecution - date time execution
type TimeExecution struct {
	Date             string  `json:"date"`
	TimeExecutionSec float64 `json:"time_execution_sec"`
}

// BoolRes - struct for bool result
type BoolRes struct {
	Result bool `json:"result"`
	TimeExecution
}

///////////////////////////////
////////// company ////////////
///////////////////////////////

// CompanyDetailRes - struct for company create
type CompanyDetailRes struct {
	Object Company `json:"object"`
	TimeExecution
}

// Result - struct of objects array
type Result struct {
	Objects    []Company `json:"objects"`
	ObjectsCnt int       `json:"objects_cnt"`
}

// CompanyListRes - struct for company list page
type CompanyListRes struct {
	Result Result `json:"result"`
	TimeExecution
}

///////////////////////////////
/////////// contract //////////
///////////////////////////////

// ContractDetailRes - struct for contract create
type ContractDetailRes struct {
	Object Contract `json:"object"`
	TimeExecution
}

// ContractListRes - struct for contract list page
type ContractListRes struct {
	Result ResultContracts `json:"result"`
	TimeExecution
}

// ResultContracts - struct of objects array
type ResultContracts struct {
	Objects    []Contract `json:"objects"`
	ObjectsCnt int        `json:"objects_cnt"`
}

///////////////////////////////
/////////// purchase //////////
///////////////////////////////

// PurchaseDetailRes - struct for company create
type PurchaseDetailRes struct {
	Object Purchase `json:"object"`
	TimeExecution
}
