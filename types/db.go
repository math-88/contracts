package types

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Company - all companies
type Company struct {
	gorm.Model
	Name            string     `gorm:"type:varchar(255); not null" json:"name"`
	Code            string     `gorm:"type:varchar(255); unique_index" json:"code"`
	ContractsSeller []Contract `gorm:"foreignkey:SellerCompanyID" json:"contracts_seller"`
	ContractsClient []Contract `gorm:"foreignkey:ClientCompanyID" json:"contracts_client"`
}

// Contract - all contracts
type Contract struct {
	gorm.Model
	SellerCompanyID uint       `gorm:"type:int(10) UNSIGNED; not null" json:"seller_company_id"`
	ClientCompanyID uint       `gorm:"type:int(10) UNSIGNED; not null" json:"client_company_id"`
	Number          string     `gorm:"type:varchar(255); unique_index; not null" json:"number"`
	DateSigned      time.Time  `gorm:"type:timestamp; not null" json:"date_signed"`
	DateValid       time.Time  `gorm:"type:timestamp; not null" json:"date_valid"`
	Credits         int        `gorm:"type:int(10) UNSIGNED; not null" json:"credits"`
	Purchases       []Purchase `json:"purchases"`
}

// Purchase - all purchases
type Purchase struct {
	gorm.Model
	ContractID uint      `gorm:"type:int(10) UNSIGNED; not null" json:"contract_id"`
	Date       time.Time `gorm:"type:timestamp; not null" json:"date"`
	Credits    int       `gorm:"type:int(10); not null" json:"credits"`
}

// Companies - companies list
// var Companies []Company
