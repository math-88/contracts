package types

///////////////////////////////
/////////// company ///////////
///////////////////////////////

// CompanyListReq - struct for company list page read
type CompanyListReq struct {
	Filter FilterCompany `json:"filter"`
}

// FilterCompany - filter for company list request
type FilterCompany struct {
	SearchWord string `json:"search_word"`
	Code       string `json:"code"`
	Limit      int    `json:"limit"`
	OrderBy    string `json:"order_by"`
	Page       int    `json:"page"`
}

// CompanyCreateReq - struct for company create
type CompanyCreateReq struct {
	Name string `json:"name"`
	Code string `json:"code"`
}

// CompanyUpdateReq - struct for company update
type CompanyUpdateReq struct {
	ID int `json:"-"`
	CompanyCreateReq
}

///////////////////////////////
////////// contract ///////////
///////////////////////////////

// ContractCreateReq - struct for contract create
type ContractCreateReq struct {
	SellerCompany int    `json:"seller_company"`
	ClientCompany int    `json:"client_company"`
	Number        string `json:"number"`
	DateSigned    string `json:"date_signed"`
	DateValid     string `json:"date_valid"`
	Credits       int    `json:"credits"`
}

// ContractListReq - struct for company list page read
type ContractListReq struct {
	Filter FilterContract `json:"filter"`
}

// FilterContract - filter for contract list request
type FilterContract struct {
	Number  string `json:"number"`
	Limit   int    `json:"limit"`
	OrderBy string `json:"order_by"`
	Page    int    `json:"page"`
}

// ContractUpdateReq - struct for contract update
type ContractUpdateReq struct {
	ID int `json:"-"`
	ContractCreateReq
}

///////////////////////////////
/////////// purchase //////////
///////////////////////////////

// PurchaseCreateReq - struct for company create
type PurchaseCreateReq struct {
	Contract    uint   `json:"contract_id"`
	Date        string `json:"date_time"`
	CreditSpend int    `json:"credits_spend"`
}
