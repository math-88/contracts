package main

import (
	"./common"
	"./db"
	"./handlers"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"
)

// ServerStart - start echo server
func ServerStart() {
	e := echo.New()

	// company
	gCompany := e.Group("/api/company/")
	gCompany.POST("create", handlers.CreateCompany)
	gCompany.GET("list", handlers.GetCompanyList)
	gCompany.GET("detail/:id", handlers.GetCompanyDetail)
	gCompany.PUT("update/:id", handlers.UpdateCompany)
	gCompany.DELETE("delete/:id", handlers.DeleteCompany)

	// contract
	gContract := e.Group("/api/contract/")
	gContract.POST("create", handlers.CreateContract)
	gContract.GET("list", handlers.GetContractList)
	gContract.GET("detail/:id", handlers.GetContractDetail)
	gContract.PUT("update/:id", handlers.UpdateContract)
	gContract.DELETE("delete/:id", handlers.DeleteContract)

	// purchase
	gPurchase := e.Group("/api/purchase/")
	gPurchase.POST("create", handlers.CreatePurchase)

	e.Logger.Fatal(e.Start(":" + common.ServerPort))
}

func main() {
	common.Init()
	db.NewDB() // CREATE NEW DB!!!
	db.Migrate()

	ServerStart()
}
