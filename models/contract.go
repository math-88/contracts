package models

import (
	"errors"
	"log"
	"math"
	"time"

	"../common"
	"../db"
	"../types"
)

// CreateContract - model create contract
func CreateContract(cReq *types.ContractCreateReq) (cRes types.ContractDetailRes, err error) {
	startTime := time.Now()

	var contract types.Contract

	contract.SellerCompanyID = uint(cReq.SellerCompany)
	contract.ClientCompanyID = uint(cReq.ClientCompany)
	contract.Credits = cReq.Credits
	contract.Number, err = db.GenerateContractKey()
	if err != nil {
		log.Println("ERROR. models.CreateContract #1", err)
		return
	}

	contract.DateSigned, err = time.Parse(common.Layout, cReq.DateSigned)
	if err != nil {
		log.Println("ERROR. models.CreateContract #2", err)
		return
	}

	contract.DateValid, err = time.Parse(common.Layout, cReq.DateValid)
	if err != nil {
		log.Println("ERROR. models.CreateContract #3", err)
		return
	}

	err = db.CreateContract(&contract)
	if err != nil {
		return
	}

	cRes.Object = contract
	cRes.Date = common.TimePrint()

	duration := time.Since(startTime)
	cRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000
	return
}

// GetContractList - model companies list
func GetContractList(clReq *types.ContractListReq) (clRes types.ContractListRes, err error) {

	var contractsAll []types.Contract

	startTime := time.Now()
	contractsAll, err = db.GetContractList(clReq)
	if err != nil {
		return
	}

	len := len(contractsAll)
	startPoint := (clReq.Filter.Page - 1) * clReq.Filter.Limit
	if startPoint > len { // error page number
		err = errors.New("Page number too much")
		log.Println("ERROR models.GetContractList #1.", err)
		return
	}
	endPoint := int(math.Min(float64(startPoint+clReq.Filter.Limit), float64(len)))
	companies := contractsAll[startPoint:endPoint]

	clRes.Result.Objects = companies
	clRes.Result.ObjectsCnt = len

	clRes.Date = common.TimePrint()
	duration := time.Since(startTime)
	clRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000

	return
}

// GetContractDetail - get contract detail
func GetContractDetail(id *uint) (cdRes types.ContractDetailRes, err error) {

	startTime := time.Now()

	var contractDetail types.Contract

	contractDetail, err = db.GetContractDetail(id)
	if err != nil {
		return
	}
	cdRes.Object = contractDetail
	cdRes.Date = common.TimePrint()
	duration := time.Since(startTime)

	cdRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000

	return
}

// UpdateContract - model update company
func UpdateContract(cuReq *types.ContractUpdateReq, id *int) (cuRes types.BoolRes, err error) {
	startTime := time.Now()

	// fmt.Printf("cuReq = %+v", cuReq)

	var contract types.Contract
	contract.ID = uint(*id)
	contract.SellerCompanyID = uint(cuReq.SellerCompany)
	contract.ClientCompanyID = uint(cuReq.ClientCompany)
	contract.Number = cuReq.Number
	contract.Credits = cuReq.Credits

	// fmt.Printf("contract = %+v", contract)

	err = db.UpdateContract(&contract)
	if err != nil {
		return
	}
	cuRes.Result = true
	cuRes.Date = common.TimePrint()
	duration := time.Since(startTime)
	cuRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000
	return
}

// DeleteContract - delete contract
func DeleteContract(id *int) (cdelRes types.BoolRes, err error) {

	var contract types.Contract

	startTime := time.Now()

	contract.ID = uint(*id)

	err = db.DeleteContract(&contract)
	if err != nil {
		return
	}
	cdelRes.Result = true
	duration := time.Since(startTime)
	cdelRes.Date = common.TimePrint()
	cdelRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000

	return
}
