package models

import (
	"errors"
	"log"
	"time"

	"../common"
	"../db"
	"../types"
)

// CreatePurchase - model create purchase
func CreatePurchase(pReq *types.PurchaseCreateReq) (cRes types.PurchaseDetailRes, err error) {
	startTime := time.Now()

	// check free credits
	var creditsFree int

	creditsFree, err = getFreeCredits(&pReq.Contract)
	if err != nil {
		log.Println("ERROR. models.CreatePurchase #1.", err)
		return
	}

	var purchase types.Purchase

	purchase.ContractID = pReq.Contract
	purchase.Date, err = time.Parse(common.Layout, pReq.Date)
	if err != nil {
		log.Println("ERROR. models.CreatePurchase #1", err)
		return
	}
	purchase.Credits = pReq.CreditSpend

	if creditsFree < purchase.Credits {
		err = errors.New("ERROR. No have credits")
		return
	}

	err = db.CreatePurchase(&purchase)
	if err != nil {
		return
	}

	cRes.Object = purchase
	cRes.Date = common.TimePrint()

	duration := time.Since(startTime)
	cRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000
	return
}

// getFreeCredits - get free credits for contract
func getFreeCredits(id *uint) (creditsFree int, err error) {

	var contractDetail types.Contract

	contractDetail, err = db.GetContractDetail(id)
	if err != nil {
		return
	}
	usedCredits, err := GetUsedCredits(id)
	if err != nil {
		return
	}
	creditsFree = contractDetail.Credits - usedCredits

	// fmt.Printf("\ncontract = %d\nusedCredits = %d\nfree = %d\n", contractDetail.Credits, usedCredits, free)

	return
}

// GetUsedCredits - get used credits for contract
func GetUsedCredits(id *uint) (sum int, err error) {
	sum, err = db.GetUsedCredits(id)

	return
}
