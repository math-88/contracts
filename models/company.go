package models

import (
	"errors"
	"log"
	"math"
	"time"

	"../common"
	"../db"
	"../types"
)

// CreateCompany - model create company
func CreateCompany(cReq *types.CompanyCreateReq) (cRes types.CompanyDetailRes, err error) {
	startTime := time.Now()

	var company types.Company
	company.Name = cReq.Name
	company.Code = cReq.Code

	err = db.CreateCompany(&company)
	if err != nil {
		return
	}

	cRes.Object = company
	cRes.Date = common.TimePrint()

	duration := time.Since(startTime)
	cRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000
	return
}

// GetCompaniesList - model companies list
func GetCompaniesList(clReq *types.CompanyListReq) (clRes types.CompanyListRes, err error) {

	var companiesAll []types.Company

	startTime := time.Now()
	companiesAll, err = db.GetCompanyList(clReq)
	if err != nil {
		return
	}

	len := len(companiesAll)
	startPoint := (clReq.Filter.Page - 1) * clReq.Filter.Limit
	if startPoint > len { // error page number
		err = errors.New("Page number too much")
		log.Println("ERROR models.GetCompaniesList #1.", err)
		return
	}
	endPoint := int(math.Min(float64(startPoint+clReq.Filter.Limit), float64(len)))
	companies := companiesAll[startPoint:endPoint]

	clRes.Result.Objects = companies
	clRes.Result.ObjectsCnt = len

	clRes.Date = common.TimePrint()
	duration := time.Since(startTime)
	clRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000

	return
}

// GetCompanyDetail - get company detail
func GetCompanyDetail(id *int) (cdRes types.CompanyDetailRes, err error) {

	var companyDetail types.Company
	startTime := time.Now()

	companyDetail, err = db.GetCompanyDetail(id)
	if err != nil {
		return
	}
	cdRes.Object = companyDetail
	duration := time.Since(startTime)
	cdRes.Date = common.TimePrint()
	cdRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000

	return
}

// UpdateCompany - model update company
func UpdateCompany(cuReq *types.CompanyUpdateReq, id *int) (cuRes types.BoolRes, err error) {
	startTime := time.Now()

	var company types.Company
	company.ID = uint(*id)
	company.Name = cuReq.Name
	company.Code = cuReq.Code

	err = db.UpdateCompany(&company)
	if err != nil {
		return
	}
	cuRes.Result = true
	cuRes.Date = common.TimePrint()
	duration := time.Since(startTime)
	cuRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000
	return
}

// DeleteCompany - delete company
func DeleteCompany(id *int) (cdelRes types.BoolRes, err error) {

	var company types.Company

	startTime := time.Now()

	if *id < 1 {
		err = errors.New("Error id")
		log.Println("ERROR. DeleteCompany #1", err)
		return
	}
	company.ID = uint(*id)

	err = db.DeleteCompany(&company)
	if err != nil {
		return
	}
	cdelRes.Result = true
	duration := time.Since(startTime)
	cdelRes.Date = common.TimePrint()
	cdelRes.TimeExecutionSec = float64(duration/time.Millisecond) / 1000

	return
}
